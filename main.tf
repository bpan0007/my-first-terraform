provider "aws" {
    region = "eu-west-3"

}
resource "aws_vpc" "development-vpc-xy" {
    cidr_block = "10.0.0.0/16"
    tags = {
        Name = "dev-vpc"
    }
}
resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development-vpc-xy.id
    cidr_block = "10.0.10.0/24"
    tags = {
        Name = "sub1-dev-vpc"
    }

}
 
